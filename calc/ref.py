from collections import defaultdict
from sys import stdin
from typing import Dict, Optional, Tuple, TypeVar, Callable, List, Any


class PointedString:
    def __init__(self, string: str, index: int = 0):
        self.string = string
        self.index = index
        self.size = len(string)

    def get_c(self) -> Optional[str]:
        if self.index >= self.size:
            return None

        res = self.string[self.index]
        self.index += 1
        return res

    def unget_c(self, amount: int = 1) -> None:
        self.index -= amount
        self.index = max(self.index, 0)


T = TypeVar('T')
S = TypeVar('S')
ParseResult = Optional[T]
Parser = Callable[[PointedString], ParseResult[T]]


def digit(string: PointedString) -> ParseResult[int]:
    char = string.get_c()

    if char is None:
        return None

    if not char.isdigit():
        return None

    return int(char)


def letter(string: PointedString) -> ParseResult[str]:
    char = string.get_c()

    if char is None:
        return None

    if not char.isalpha():
        return None

    return char


def parse_char(string: PointedString) -> ParseResult[str]:
    char = string.get_c()
    return char


def many_parser(parser: Parser[T]) -> Parser[List[T]]:

    def res_parser(string: PointedString) -> ParseResult[List[T]]:

        result = []
        while True:
            index = string.index
            res = parser(string)
            if res is None:
                string.index = index
                break
            result.append(res)
        return result

    return res_parser


def many1_parser(parser: Parser[T]) -> Parser[List[T]]:
    many_par = many_parser(parser)

    def res_parser(string: PointedString) -> ParseResult[List[T]]:
        res = many_par(string)
        return res if res != [] else None
    return res_parser


def create_number(lst: List[int]) -> int:
    res = 0
    for number in lst:
        res = res * 10 + number
    return res


def number_parser() -> Parser[int]:
    parser = many1_parser(digit)

    def res_parser(string: PointedString) -> ParseResult[int]:

        result = parser(string)
        if result is None:
            return None

        return create_number(result)

    return res_parser


def oneofchar_parser(characters: str) -> Parser[str]:
    char_set = set(characters)

    def res_parser(string: PointedString) -> ParseResult[str]:
        char = string.get_c()
        if char is None:
            return None

        if char not in char_set:
            return None

        return char

    return res_parser


def sequential_parser(parsers: List[Parser[Any]]) -> Parser[Tuple[Any, ...]]:

    def res_parser(string: PointedString) -> ParseResult[Tuple[Any, ...]]:
        res_list = []

        for parser in parsers:
            element_res = parser(string)
            if element_res is None:
                return None
            res_list.append(element_res)
        return tuple(res_list)
    return res_parser


def sequential_parser_ignore(parsers: List[Tuple[Parser[Any], bool]]) -> Parser[Tuple[Any, ...]]:

    def res_parser(string: PointedString) -> ParseResult[Tuple[Any, ...]]:
        res_list = []

        for parser, include in parsers:
            element_res = parser(string)
            if element_res is None:
                return None
            if include:
                res_list.append(element_res)
        return tuple(res_list)
    return res_parser


def spaces(string: PointedString) -> ParseResult[str]:
    parser = many_parser(oneofchar_parser(' '))
    res = parser(string)
    if res is None:
        return res
    return ''.join(res)


def res_transformer(parser: Parser[T], transformer: Callable[[T], S]) -> Parser[S]:
    def res_parser(string: PointedString) -> ParseResult[S]:

        res = parser(string)

        if res is None:
            return None

        return transformer(res)
    return res_parser


def binary_operation(value: Tuple[Any, ...]) -> Optional[int]:
    a, operator, b = value

    if operator == "+":
        return a + b
    if operator == "-":
        return a - b
    if operator == "*":
        return a * b
    if operator == "/":
        return a // b

    return None


def oneof(lst: List[Parser[Any]]) -> Parser[Any]:

    def res_parser(string: PointedString) -> ParseResult[Any]:
        for parser in lst:
            index = string.index
            res = parser(string)

            if res is not None:
                return res

            string.index = index
            continue

    return res_parser


def keyword_parser(keyword: str) -> Parser[str]:
    def res_parser(string: PointedString) -> ParseResult[str]:
        for ch in keyword:
            character = string.get_c()
            if character is None:
                return None
            if character != ch:
                return None
        return keyword
    return res_parser


varname_parser = res_transformer(
    many1_parser(letter), lambda x: ''.join(x))


def variable_parser(memory: Dict[str, int]) -> Parser[int]:
    def res_parser(string: PointedString) -> ParseResult[int]:
        varname = varname_parser(string)
        if varname is None:
            return None

        if varname not in memory:
            return None
        return memory[varname]
    return res_parser


def assignment(memory: Dict[str, int]) -> Callable[[Tuple[Any, ...]], int]:
    def transformer(value_tuple: Tuple[Any, ...]) -> int:
        a, value = value_tuple
        memory[a] = value
        return value
    return transformer


if __name__ == "__main__":

    memory = {}
    for line in stdin:
        pointed_string = PointedString(line.strip())

        value_parser = oneof([
            number_parser(),
            variable_parser(memory),
        ])

        bin_op_sequence = [
            (value_parser, True),
            (spaces, False),
            (oneofchar_parser("+-*/"), True),
            (spaces, False)
        ]

        element = res_transformer(sequential_parser_ignore([
            (spaces, False),
            (value_parser, True),
            (spaces, False)
        ]), lambda x: x[0])

        bin_op_parser = res_transformer(
            sequential_parser_ignore(bin_op_sequence), binary_operation)

        expression_parser = oneof([
            bin_op_parser,
            element
        ])

        bin_op_sequence.append((expression_parser, True))

        assignment_parser = res_transformer(sequential_parser_ignore([
            (varname_parser, True),
            (spaces, False),
            (keyword_parser("="), False),
            (spaces, False),
            (expression_parser, True)
        ]), assignment(memory))

        parser = oneof([
            expression_parser,
            assignment_parser
        ])

        res = parser(pointed_string)
        print(res)
